package com.ppis.ubb.models;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.UbbUser;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCredentials {
    UbbUser user;
    Credentials credentials;
}
