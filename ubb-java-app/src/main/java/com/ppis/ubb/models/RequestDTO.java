package com.ppis.ubb.models;

import com.ppis.ubb.entities.Classroom;
import com.ppis.ubb.entities.Course;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@Data
public class RequestDTO {
    private Long requestId;
    private String requestType;
    private Classroom classroom;
    private Course course;
    private String username;
}
