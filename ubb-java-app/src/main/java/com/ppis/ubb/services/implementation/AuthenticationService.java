package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.services.interfaces.IAuthenticationService;
import com.ppis.ubb.services.interfaces.ICredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService implements IAuthenticationService {
    @Autowired
    ICredentialsService credentialsService;
    public Role canAuthenticate(Credentials credentials){
        Credentials existingCredentials = credentialsService.loadCredentialsByUsername(credentials.getUsername());
        if (existingCredentials != null) {
            if (existingCredentials.isLocked())
                return null;
            if (credentials.getPassword().equals(existingCredentials.getPassword())) {
                credentialsService.update(existingCredentials);
                return existingCredentials.getRole();
            }
        }
        return null;
    }
}
