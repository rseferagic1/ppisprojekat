package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Classroom;
import com.ppis.ubb.repository.ClassroomRepository;
import com.ppis.ubb.services.interfaces.IClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClassroomService implements IClassroomService {

    @Autowired
    private ClassroomRepository classroomRepository;

    @Override
    public List<Classroom> getAllClassrooms() {
        return (List<Classroom>) classroomRepository.findAll();
    }

    @Override
    public List<Classroom> getAllAvailableClassrooms() {
        return classroomRepository.findByInUse(false);
    }
}
