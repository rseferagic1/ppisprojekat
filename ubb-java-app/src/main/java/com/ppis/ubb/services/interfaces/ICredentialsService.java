package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Credentials;

public interface ICredentialsService {
    Credentials loadCredentialsByUsername(String username);
    void update(Credentials newCredentials);
}
