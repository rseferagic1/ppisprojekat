package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Permission;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.entities.UbbUser;
import com.ppis.ubb.repository.CredentialsRepository;
import com.ppis.ubb.repository.PermissionRepository;
import com.ppis.ubb.repository.RoleRepository;
import com.ppis.ubb.services.interfaces.IRolesService;
import org.apache.catalina.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService implements IRolesService {
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PermissionRepository permissionRepository;
    @Override
    public List<Role> findAll() {
        return (List<Role>) roleRepository.findAll();
    }
    @Override
    public Role save(String role) {
        Role newRole = new Role();
        newRole.setDescription(role);
        return roleRepository.save(newRole);
    }
    @Override
    public Permission savePermission(String role) {
        Permission newRole = new Permission();
        newRole.setDescription(role);
        return permissionRepository.save(newRole);
    }
    @Override
    public Role findByDescription(String role) {

        return roleRepository.findByDescription(role);
    }
}
