package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Classroom;

import java.util.List;

public interface IClassroomService {
    List<Classroom> getAllClassrooms();
    List<Classroom> getAllAvailableClassrooms();
}
