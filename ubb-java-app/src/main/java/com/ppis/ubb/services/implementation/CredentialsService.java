package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.repository.CredentialsRepository;
import com.ppis.ubb.services.interfaces.ICredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CredentialsService implements ICredentialsService {
    @Autowired
    CredentialsRepository credentialsRepository;
    @Override
    public Credentials loadCredentialsByUsername(String username) {
        return credentialsRepository.findByUsername(username);
    }
    @Override
    public void update(Credentials newCredentials) {
        Credentials existing = credentialsRepository.findById(newCredentials.getCredentialId()).orElse(null);
        if (existing != null && existing.getRole() != newCredentials.getRole()) {
            existing.setRole(newCredentials.getRole());
            credentialsRepository.save(existing);
        } else if (existing != null) {
            existing.setLastLogin(new Date());
            credentialsRepository.save(existing);
        }
    }
}
