package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Request;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.entities.UbbUser;
import com.ppis.ubb.repository.CredentialsRepository;
import com.ppis.ubb.repository.RequestRepository;
import com.ppis.ubb.repository.RoleRepository;
import com.ppis.ubb.repository.UbbUserRepository;
import com.ppis.ubb.services.interfaces.IUbbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUbbUserService {

    @Autowired
    private UbbUserRepository ubbUserRepository;

    @Autowired
    private CredentialsRepository credentialsRepository;

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public List<UbbUser> findAll() {
        return (List<UbbUser>) ubbUserRepository.findAll();
    }
    @Override
    public UbbUser saveNewUser(UbbUser user, Credentials credentials) {
        credentials = credentialsRepository.save(credentials);
        user.setCredentials(credentials);
        return ubbUserRepository.save(user);
    }

    @Override
    public UbbUser getUserDetailsByUsername(String username) {
        Credentials credentials = credentialsRepository.findByUsername(username);
        return ubbUserRepository.findByCredentials(credentials);
    }

    @Override
    public List<Request> getUserRequestsByUsername(String username) {
        return requestRepository.findByUser(getUserDetailsByUsername(username));
    }
}
