package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Permission;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.entities.UbbUser;

import java.util.List;

public interface IRolesService {
    List<Role> findAll();
    Role save(String role);

    Role findByDescription(String desc);
    Permission savePermission(String role);
}
