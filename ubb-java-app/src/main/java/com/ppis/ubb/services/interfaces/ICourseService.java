package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Course;

import java.util.List;

public interface ICourseService {
    List<Course> getAllCourses();
}
