package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Role;

public interface IAuthenticationService {
    Role canAuthenticate(Credentials credentials) throws Exception;

}
