package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Course;
import com.ppis.ubb.repository.CourseRepository;
import com.ppis.ubb.services.interfaces.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService implements ICourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public List<Course> getAllCourses() {
        return (List<Course>) courseRepository.findAll();
    }
}
