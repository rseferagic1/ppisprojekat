package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Request;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.entities.UbbUser;

import java.util.List;

public interface IUbbUserService {
    List<UbbUser> findAll();
    UbbUser saveNewUser(UbbUser user, Credentials credentials);
    UbbUser getUserDetailsByUsername(String username);
    List<Request> getUserRequestsByUsername(String username);
}
