package com.ppis.ubb.services.implementation;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Request;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.models.RequestDTO;
import com.ppis.ubb.repository.*;
import com.ppis.ubb.services.interfaces.IRequestService;
import com.ppis.ubb.services.interfaces.IRolesService;
import com.ppis.ubb.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RequestService implements IRequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private UbbUserRepository userRepository;

    @Autowired
    private CredentialsRepository credentialsRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private IRolesService roleService;
    @Override
    public List<Request> getAllRequests() {
        return (List<Request>) requestRepository.findAll();
    }

    @Override
    public Request saveRequest(RequestDTO request) {
        if (!Constants.getAllRequestTypesAsList().contains(request.getRequestType())) {
            request.setCourse(null);
            request.setClassroom(null);
        }

        Request newRequest = new Request();
        newRequest.setRequestDate(Date.from(new Date().toInstant()));
        newRequest.setRequestStatus("Open");
        newRequest.setRequestType(request.getRequestType());
        newRequest.setUser(userRepository.findByCredentials(credentialsRepository.findByUsername(request.getUsername())));
        newRequest.setCourse(request.getCourse());
        newRequest.setStudentName(request.getUsername());
        return requestRepository.save(newRequest);
    }

    @Override
    public List<Request> getProfessorVisibleRequestsFromStudents(String username) {
        return requestRepository.getRequestsByProfessorUsername(username);
    }

    @Override
    public void batchUpdateRequests(String status, List<Request> requests) {
        for (Request request : requests) {
            request.setRequestStatus(status);
            if (request.getRequestType() == "Tutor of specified course") {
                Credentials existing = credentialsRepository.findByUsername(request.getStudentName());
                existing.setRole(roleService.findByDescription("Tutor"));
                credentialsRepository.save(existing);
            }
            requestRepository.save(request);
        }
    }
}
