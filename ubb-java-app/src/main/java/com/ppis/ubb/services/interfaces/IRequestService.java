package com.ppis.ubb.services.interfaces;

import com.ppis.ubb.entities.Request;
import com.ppis.ubb.models.RequestDTO;

import java.util.List;

public interface IRequestService {
    List<Request> getAllRequests();
    Request saveRequest(RequestDTO request);
    List<Request> getProfessorVisibleRequestsFromStudents(String username);
    void batchUpdateRequests(String status, List<Request> requests);
}
