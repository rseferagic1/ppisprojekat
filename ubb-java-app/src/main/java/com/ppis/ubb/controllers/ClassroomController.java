package com.ppis.ubb.controllers;

import com.ppis.ubb.entities.Classroom;
import com.ppis.ubb.services.interfaces.IClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/classrooms")
public class ClassroomController {

    @Autowired
    private IClassroomService classroomService;

    @GetMapping
    List<Classroom> getAllClassrooms() {
        return classroomService.getAllClassrooms();
    }

    @GetMapping("/available")
    List<Classroom> getAllAvailableClassrooms() {
        return classroomService.getAllAvailableClassrooms();
    }
}
