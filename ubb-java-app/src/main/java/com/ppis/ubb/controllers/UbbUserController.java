package com.ppis.ubb.controllers;


import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Request;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.entities.UbbUser;
import com.ppis.ubb.models.UserCredentials;
import com.ppis.ubb.services.interfaces.IRolesService;
import com.ppis.ubb.services.interfaces.IUbbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UbbUserController {

    @Autowired
    private IUbbUserService ubbUserService;

    @GetMapping
    public List<UbbUser> getAllUsers(){
        return ubbUserService.findAll();
    }
    @PostMapping
    public UbbUser createUser(@RequestBody UserCredentials userCredentials){
        SimpleDateFormat format = new SimpleDateFormat("M/d/yy, h:mm a");
        userCredentials.getUser().setDateCreated(format.format(new Date()));
        return ubbUserService.saveNewUser(userCredentials.getUser(), userCredentials.getCredentials()); }

    @GetMapping("/{username}")
    public UbbUser getUserDetails(@PathVariable String username) {
        return ubbUserService.getUserDetailsByUsername(username);
    }

    @GetMapping("/requests/{username}")
    public List<Request> getUserRequests(@PathVariable String username) {
        return ubbUserService.getUserRequestsByUsername(username);
    }
}
