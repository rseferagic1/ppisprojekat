package com.ppis.ubb.controllers;

import com.ppis.ubb.entities.Classroom;
import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.services.interfaces.IClassroomService;
import com.ppis.ubb.services.interfaces.ICredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/credentials")
public class CredentialsController {

    @Autowired
    private ICredentialsService credentialsService;

    @PostMapping
    void updateCredentials(@RequestBody Credentials newCredentials) {
        credentialsService.update(newCredentials);
    }
}
