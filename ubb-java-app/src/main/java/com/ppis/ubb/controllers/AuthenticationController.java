package com.ppis.ubb.controllers;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.services.interfaces.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    @Autowired
    IAuthenticationService authenticationService;
    @PostMapping
    public Role isAuthenticated(@RequestBody Credentials credentials) throws Exception{
        return authenticationService.canAuthenticate(credentials);
    }
}
