package com.ppis.ubb.controllers;

import com.ppis.ubb.entities.Request;
import com.ppis.ubb.models.RequestDTO;
import com.ppis.ubb.services.interfaces.IRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/requests")
@RestController
public class RequestController {

    @Autowired
    private IRequestService requestService;

    @PostMapping
    public Request saveRequest(@RequestBody RequestDTO request) {
        return requestService.saveRequest(request);
    }

    @PutMapping("/{status}")
    public void updateRequest(@PathVariable String status,  @RequestBody List<Request> requests) {
        requestService.batchUpdateRequests(status, requests);
    }

    @GetMapping("/course-requests/professor/{username}")
    public List<Request> getProfessorVisibleRequestsFromStudents(@PathVariable String username) {
        return requestService.getProfessorVisibleRequestsFromStudents(username);
    }

    @GetMapping
    public List<Request> getAllRequests() {
        return requestService.getAllRequests();
    }
}
