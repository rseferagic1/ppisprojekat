package com.ppis.ubb.controllers;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.Permission;
import com.ppis.ubb.entities.Role;
import com.ppis.ubb.entities.UbbUser;
import com.ppis.ubb.repository.PermissionRepository;
import com.ppis.ubb.repository.RoleRepository;
import com.ppis.ubb.services.interfaces.IAuthenticationService;
import com.ppis.ubb.services.interfaces.IRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/roles")
public class RolesController {
    @Autowired
    IRolesService rolesService;

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PermissionRepository permissionRepository;
    @GetMapping
    public List<Role> getAllRoles(){
        return rolesService.findAll();
    }
   @PostMapping
    public Role createRole(@RequestBody String role){ return rolesService.save(role); }
    @PostMapping("/permission")
    public Permission createPermission(@RequestBody String role){ return rolesService.savePermission(role); }
    @GetMapping("/{roleId}/permissions")
    public List<Permission> getPermissionsByRole(@PathVariable Long roleId){
        return roleRepository.findById(roleId).orElseThrow().getPermission();
    }
    @GetMapping("/permissions")
    public List<Permission> getAllPermissions(){
        return (List<Permission>)permissionRepository.findAll();
    }
}
