package com.ppis.ubb.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long requestId;

    private String requestType;
    private String requestStatus;
    private Date requestDate;
    private String studentName;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Course course;

    @ManyToOne(cascade = CascadeType.MERGE)
    private UbbUser user;
}
