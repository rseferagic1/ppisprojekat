package com.ppis.ubb.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long studentId;
    @NotNull
    private String indexNumber;
    @OneToOne
    private UbbUser user;

    @ManyToMany(targetEntity = Course.class, cascade = CascadeType.MERGE)
    private List<Course> courses;
}
