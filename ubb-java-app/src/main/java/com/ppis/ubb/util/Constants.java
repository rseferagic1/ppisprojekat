package com.ppis.ubb.util;

import com.ppis.ubb.entities.Request;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static class RequestTypes {
        public static final String RESERVE_CLASSROOM = "Reserve classroom for the exam";
        public static final String PROFESSOR_OF_COURSE = "Professor of specified course";
        public static final String TUTOR_OF_COURSE = "Tutor of specified course";
        public static final String ENROLL_IN_COURSE = "Enroll in specified course";
    }

    public static List<String> getAllRequestTypesAsList() {
        return Arrays.asList(RequestTypes.PROFESSOR_OF_COURSE, RequestTypes.RESERVE_CLASSROOM, RequestTypes.ENROLL_IN_COURSE, RequestTypes.TUTOR_OF_COURSE);
    }
}
