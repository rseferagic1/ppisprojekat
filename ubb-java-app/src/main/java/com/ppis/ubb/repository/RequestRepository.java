package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Request;
import com.ppis.ubb.entities.UbbUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RequestRepository extends CrudRepository<Request, Long> {
    List<Request> findByUser(UbbUser user);

    @Query(value = "SELECT request_id, request_type, request_status, request_date, r.student_name, r.course_course_id, r.user_user_id\n" +
            "\tFROM request r\n" +
            "\tJOIN course c ON c.course_id = r.course_course_id\n" +
            "\tJOIN ubb_user stud ON stud.user_id = r.user_user_id\n" +
            "\tJOIN professor p ON p.professor_id = c.professor_id\n" +
            "\tJOIN ubb_user u ON u.user_id = p.user_user_id\n" +
            "\tJOIN credentials cr ON u.credentials_credential_id = cr.credential_id WHERE username=?1", nativeQuery = true)
    List<Request> getRequestsByProfessorUsername(String username);
}
