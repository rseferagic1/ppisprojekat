package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Classroom;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClassroomRepository extends CrudRepository<Classroom,Long> {
    List<Classroom> findByInUse(Boolean inUse);
}
