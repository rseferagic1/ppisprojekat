package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Report;
import org.springframework.data.repository.CrudRepository;

public interface ReportRepository extends CrudRepository<Report, Long> {
}
