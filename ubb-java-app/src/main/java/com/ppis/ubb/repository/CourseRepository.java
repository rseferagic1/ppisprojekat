package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, Long> {
}
