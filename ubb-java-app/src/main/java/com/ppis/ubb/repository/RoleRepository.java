package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
    Role findByDescription(String description);
}
