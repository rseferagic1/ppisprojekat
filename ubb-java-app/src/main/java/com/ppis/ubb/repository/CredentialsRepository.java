package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Credentials;
import org.springframework.data.repository.CrudRepository;

public interface CredentialsRepository extends CrudRepository<Credentials, Long> {
    Credentials findByUsername(String username);
}
