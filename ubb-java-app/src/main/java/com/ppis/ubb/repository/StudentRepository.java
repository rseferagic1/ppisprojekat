package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Long> {
}
