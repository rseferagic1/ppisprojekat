package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Permission;
import org.springframework.data.repository.CrudRepository;

public interface PermissionRepository extends CrudRepository<Permission, Long> {

}
