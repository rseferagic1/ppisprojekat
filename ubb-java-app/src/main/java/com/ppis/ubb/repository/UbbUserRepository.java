package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Credentials;
import com.ppis.ubb.entities.UbbUser;
import org.springframework.data.repository.CrudRepository;

public interface UbbUserRepository extends CrudRepository<UbbUser, Long> {
    UbbUser findByCredentials(Credentials credentials);

}
