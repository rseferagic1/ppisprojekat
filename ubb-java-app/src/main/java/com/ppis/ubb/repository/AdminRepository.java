package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Admin;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Admin, Long> {
}
