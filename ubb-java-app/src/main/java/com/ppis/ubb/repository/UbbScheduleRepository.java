package com.ppis.ubb.repository;

import com.ppis.ubb.entities.UbbSchedule;
import org.springframework.data.repository.CrudRepository;

public interface UbbScheduleRepository extends CrudRepository<UbbSchedule, Long> {
}
