package com.ppis.ubb.repository;

import com.ppis.ubb.entities.Professor;
import org.springframework.data.repository.CrudRepository;

public interface ProfessorRepository extends CrudRepository<Professor, Long> {
}
