import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginService } from './login.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
  })

export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loginSuccess = false;

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private loginService: LoginService
    ) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: [null, Validators.required],
            password: [null, Validators.required]
        });
        this.loginForm.valueChanges.subscribe(x => this.loginForm.clearValidators());
    }

    public submit(): void {
        if (this.loginForm.valid) {
            this.loginService.authenticate(this.loginForm.value).subscribe(res => {
            if (res) {
                localStorage.setItem('username', this.loginForm.value.username);
                localStorage.setItem('role', res.roleId.toString());
                switch (res.roleId) {
                    case 100:
                        this.router.navigateByUrl('/student');
                        return;
                    case 101:
                        this.router.navigateByUrl('/professor');
                        return;
                    case 102:
                        this.router.navigateByUrl('/admin');
                        return;
                    case 103:
                        this.router.navigateByUrl('/admin');
                        return;
                    default:
                        return;
                    }
            } else {
                this.loginForm.setErrors({badCredentials: true});
            }
        });
        }
    }
}
