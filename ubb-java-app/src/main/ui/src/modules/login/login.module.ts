import { LoginComponent } from './login.component';
import { NgModule } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { LoginService } from './login.service';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
      LoginComponent
    ],
    imports: [
      MatFormFieldModule,
      MatSelectModule,
      MatInputModule,
      MatButtonModule,
      MatMenuModule,
      MatSidenavModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule
    ],
    providers: [LoginService]
  })
  export class LoginModule { }
