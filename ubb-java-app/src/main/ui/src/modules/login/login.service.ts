import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { CredentialsDTO } from 'src/core/models/credentials-dto';
import { RoleDTO } from 'src/core/models/role-dto';

@Injectable()
export class LoginService {
    constructor(
      protected http: HttpClient
    ) {}

    public authenticate(credentials: CredentialsDTO): Observable<RoleDTO> {
      return this.http.post<RoleDTO>(environment.url +  'api/auth', credentials);
    }
}
