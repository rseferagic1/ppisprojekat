import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-access-management',
  templateUrl: './access-management.component.html',
  styleUrls: ['./access-management.component.scss']
})
export class AccessManagementComponent implements OnInit {

  constructor() { }
  columnDefs = [
    {headerName: 'ID', field: 'id' },
    {headerName: 'Username', field: 'username'},
    {headerName: 'Email', field: 'email'},
    {headerName: 'Date / Time', field: 'date'},
    {headerName: 'Action description', field: 'action', resizable: true}
];

rowData = [
    { id: 1, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '20.01.2020. 12:58', action: 'Logged in' },
  { id: 2, username: 'jlewis1', email: 'jlewis1@etf.unsa.ba', date: '20.01.2020. 13:24', action: 'Logged in' },
  { id: 3, username: 'kyakimenko1', email: 'kyakimenko1@etf.unsa.ba', date: '20.01.2020. 13:38', action: 'Logged out' },
  { id: 4, username: 'amarlin1', email: 'amarlin1@etf.unsa.ba', date: '20.01.2020. 15:44', action: 'Reset password' },
  { id: 5, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:21', action: 'Created new permission - Create reports' },
  { id: 6, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:26', action: 'Assigned permission - Create reports - to Professor role' },
  { id: 7, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:40', action: 'Granted user 17829 new permission - Tutor' },
  { id: 8, username: 'jbellen1', email: 'jbellen1@etf.unsa.ba', date: '21.01.2020. 07:56', action: 'Granted user 18923 new permission - Professor' },
  { id: 9, username: 'mcarev1', email: 'mcarev1@etf.unsa.ba', date: '21.01.2020. 08:18', action: 'Added new user - kkaleb1' },
  { id: 10, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '21.01.2020. 08:19', action: 'Added new user - mmirna1' },
  { id: 11, username: 'ddean1', email: 'ddean1@etf.unsa.ba', date: '21.01.2020. 09:23', action: 'Revoke permission - Totor - user 18929' },
  { id: 12, username: 'amax1', email: 'amax1@etf.unsa.ba', date: '21.01.2020. 11:33', action: 'Add new role - Guest' },
  { id: 13, username: 'amax1', email: 'amax1@etf.unsa.ba', date: '21.01.2020. 12:03', action: 'Logged in' },
  { id: 14, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '21.01.2020. 12:08', action: 'Logged in' },
  { id: 15, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '20.01.2020. 12:58', action: 'Logged out' },
  { id: 16, username: 'jlewis1', email: 'jlewis1@etf.unsa.ba', date: '20.01.2020. 13:24', action: 'Logged in' },
  { id: 17, username: 'kyakimenko1', email: 'kyakimenko1@etf.unsa.ba', date: '20.01.2020. 13:38', action: 'Logged in' },
  { id: 18, username: 'amarlin1', email: 'amarlin1@etf.unsa.ba', date: '20.01.2020. 15:44', action: 'Logged out' },
  { id: 19, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:21', action: 'Logged out' },
  { id: 20, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:26', action: 'Add new role - Schedule manager' },
  { id: 21, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:40', action: 'Logged in' },

];
  ngOnInit(): void {
  }

}
