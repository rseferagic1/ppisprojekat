import { Component, OnInit, ViewChild } from '@angular/core';
import { readProperty } from 'src/core/util/helper-functions';
import { TableHeader } from 'src/core/models/table-header';
import { MatTableDataSource } from '@angular/material/table';
import { RequestDTO } from 'src/core/models/request-dto';
import { MatPaginator } from '@angular/material/paginator';
import { AdminService } from '../admin.service';
import { SelectionModel } from '@angular/cdk/collections';
import { PopupService } from 'src/core/popup/popup.service';
import { concatMap } from 'rxjs/operators';

@Component({
  selector: 'app-admin-requests',
  templateUrl: './admin-requests.component.html',
  styleUrls: ['./admin-requests.component.scss']
})
export class AdminRequestsComponent implements OnInit {

  readProperty: (obj: any, field: string) => any = readProperty;

  columnDef: TableHeader[] = [
    { field: 'requestId', headerName: 'Request #' },
    { field: 'studentName', headerName: 'Requested by' },
    { field: 'requestDate', headerName: 'Requested at'},
    { field: 'requestType', headerName: 'Request type'},
    { field: 'requestStatus', headerName: 'Status'},
  ];

  dataSource: MatTableDataSource<RequestDTO>;
  displayedColumns: string[] = [];
  selection = new SelectionModel<RequestDTO>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private adminService: AdminService,
    private popupService: PopupService
  ) {}

  ngOnInit() {
    this.displayedColumns = ['select'].concat(this.columnDef.map(column => column.field));
    this.adminService.getRequests().subscribe(
      (res: RequestDTO[]) => {
        this.dataSource = new MatTableDataSource<RequestDTO>(res);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  public isAllSelected(): boolean {
    if (!this.dataSource) {
      return false;
    }
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  public masterToggle(): void {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  public checkboxLabel(row?: RequestDTO): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.requestId + 1}`;
  }

  public noneSelected(): boolean {
    return this.selection.selected.length < 1;
  }

  public batchUpdateRequests(status: string = 'Approved')  {
    const toUpdate: RequestDTO[] = this.selection.selected.filter(request => request.requestStatus === 'Open');

    if (toUpdate.length !== this.selection.selected.length) {
      this.popupService.requestsWarnPopup();
    }

    this.adminService.batchUpdateRequests(status, toUpdate).pipe(
      concatMap(
        () => this.adminService.getRequests()
      )
    ).subscribe(
      (res: RequestDTO[]) => {
        this.dataSource = new MatTableDataSource<RequestDTO>(res);
        this.selection = new SelectionModel<RequestDTO>(true, []);
      }
    );
  }
}
