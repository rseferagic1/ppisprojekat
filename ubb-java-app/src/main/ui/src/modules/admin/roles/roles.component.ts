import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from '../admin.service';
import { PopupService } from 'src/core/popup/popup.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
  })

export class RolesComponent implements OnInit {

    newRoleForm: FormGroup;
    newPermissionForm: FormGroup;
    loginSuccess = false;
    roles = [];
    permissions = [];

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private adminService: AdminService,
        private popupService: PopupService) {
    }

    ngOnInit() {
        this.newRoleForm = this.formBuilder.group({
            role: [null, Validators.required]
        });
        this.newPermissionForm = this.formBuilder.group({
            permission: [null, Validators.required],
            role: [null, Validators.required]
        });
    }
    getRoles() {
        this.adminService.getAllRoles().subscribe(res => {
            this.roles = res;
            this.roles.forEach(el => {
                this.adminService.getPermissionsByRoleId(el.roleId).subscribe(res => {
                    el.permission = res;
                })
            })

        });
    }

    createRole(): void {
      this.adminService.createNewRole(this.newRoleForm.get('role').value);
    }
    createPermission(): void {
      this.adminService.createNewPermission(this.newPermissionForm.get('permission').value);
    }
    change(role) {
        this.popupService.changePermissionsPopup(role);
    }
}
