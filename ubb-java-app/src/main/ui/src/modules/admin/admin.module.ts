import { NgModule } from '@angular/core';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatRadioModule } from '@angular/material/radio';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminService } from './admin.service';
import { RouterModule } from '@angular/router';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatCommonModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminRoutingModule } from './admin-routing.module';
import { CoreModule } from 'src/core/core.module';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';
import { AdminRequestsComponent } from './admin-requests/admin-requests.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RequestsWarnPopupComponent } from 'src/core/popup/requests-warn-popup/requests-warn-popup.component';
import { StatisticsComponent } from './statistics/statistics/statistics.component';
import { RequestStatisticsComponent } from './statistics/request-statistics/request-statistics.component';
import { HistoryComponent } from './history/history.component';
import { AgGridModule } from 'ag-grid-angular';
import {MatCardModule} from '@angular/material/card';
import { AccessManagementComponent } from './access-management/access-management.component';

@NgModule({
    declarations: [
      AdminComponent,
      AdminHomeComponent,
      UsersComponent,
      RolesComponent,
      AdminRequestsComponent,
      StatisticsComponent,
      RequestStatisticsComponent,
      HistoryComponent,
      AccessManagementComponent
    ],
    entryComponents: [RequestsWarnPopupComponent],
    imports: [
      MatFormFieldModule,
      MatSelectModule,
      MatInputModule,
      MatButtonModule,
      MatMenuModule,
      MatSidenavModule,
      ReactiveFormsModule,
      FormsModule,
      CommonModule,
      RouterModule,
      MatExpansionModule,
      MatRadioModule,
      MatCommonModule,
      MatToolbarModule,
      MatTabsModule,
      AdminRoutingModule,
      CoreModule,
      MatToolbarModule,
      MatTableModule,
      MatPaginatorModule,
      MatCheckboxModule,
      AgGridModule,
      MatCardModule
    ],
    providers: [AdminService]
  })
  export class AdminModule { }
