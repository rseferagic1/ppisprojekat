import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  constructor() { }
  columnDefs = [
    {headerName: 'ID', field: 'id' },
    {headerName: 'User ID', field: 'userId' },
    {headerName: 'Username', field: 'username'},
    {headerName: 'Email', field: 'email'},
    {headerName: 'Date / Time', field: 'date'},
    {headerName: 'Action description', field: 'action', resizable: true}
];

rowData = [
    { id: 1, userId: 17827, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '20.01.2020. 12:58', action: 'Request: Tutor - RM' },
  { id: 2, userId: 17214, username: 'jlewis1', email: 'jlewis1@etf.unsa.ba', date: '20.01.2020. 13:24', action: 'Request: Professor - RM' },
  { id: 3, userId: 10023, username: 'kyakimenko1', email: 'kyakimenko1@etf.unsa.ba', date: '20.01.2020. 13:38', action: 'Request: Enroll - PIS' },
  { id: 4, userId: 18827, username: 'amarlin1', email: 'amarlin1@etf.unsa.ba', date: '20.01.2020. 15:44', action: 'Request: Tutor - RM' },
  { id: 5, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:21', action: 'Request: 23A1 - APPROVED' },
  { id: 6, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:26', action: 'Request: 23A2 - ON HOLD' },
  { id: 7, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:40', action: 'Request: 24B1 - APPROVED' },
  { id: 8, userId: 19017, username: 'jbellen1', email: 'jbellen1@etf.unsa.ba', date: '21.01.2020. 07:56', action: 'Request: Enroll - RM' },
  { id: 9, userId: 17807, username: 'mcarev1', email: 'mcarev1@etf.unsa.ba', date: '21.01.2020. 08:18', action: 'Request: Enroll - RM' },
  { id: 10, userId: 17347, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '21.01.2020. 08:19', action: 'Request: Tutor - PIS' },
  { id: 11, userId: 12844, username: 'ddean1', email: 'ddean1@etf.unsa.ba', date: '21.01.2020. 09:23', action: 'Request: Classroom - SV1' },
  { id: 12, userId: 17117, username: 'amax1', email: 'amax1@etf.unsa.ba', date: '21.01.2020. 11:33', action: 'Request: 19N0 - DECLINED' },
  { id: 13, userId: 17117, username: 'amax1', email: 'amax1@etf.unsa.ba', date: '21.01.2020. 12:03', action: 'Request: 90K1 - APPROVED' },
  { id: 14, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '21.01.2020. 12:08', action: 'Request: Tutor - RM' },
  { id: 15, userId: 17827, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '20.01.2020. 12:58', action: 'Request: Tutor - RM' },
  { id: 16, userId: 17214, username: 'jlewis1', email: 'jlewis1@etf.unsa.ba', date: '20.01.2020. 13:24', action: 'Request: Professor - RM' },
  { id: 17, userId: 10023, username: 'kyakimenko1', email: 'kyakimenko1@etf.unsa.ba', date: '20.01.2020. 13:38', action: 'Request: Enroll - PIS' },
  { id: 18, userId: 18827, username: 'amarlin1', email: 'amarlin1@etf.unsa.ba', date: '20.01.2020. 15:44', action: 'Request: Tutor - RM' },
  { id: 19, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:21', action: 'Request: 23A1 - APPROVED' },
  { id: 20, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:26', action: 'Request: 23A2 - ON HOLD' },
  { id: 21, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '20.01.2020. 17:40', action: 'Request: 24B1 - APPROVED' },
  { id: 22, userId: 19017, username: 'jbellen1', email: 'jbellen1@etf.unsa.ba', date: '21.01.2020. 07:56', action: 'Request: Enroll - RM' },
  { id: 23, userId: 17807, username: 'mcarev1', email: 'mcarev1@etf.unsa.ba', date: '21.01.2020. 08:18', action: 'Request: Enroll - RM' },
  { id: 24, userId: 17347, username: 'ccrul1', email: 'ccrul1@etf.unsa.ba', date: '21.01.2020. 08:19', action: 'Request: Tutor - PIS' },
  { id: 25, userId: 12844, username: 'ddean1', email: 'ddean1@etf.unsa.ba', date: '21.01.2020. 09:23', action: 'Request: Classroom - SV1' },
  { id: 26, userId: 17117, username: 'amax1', email: 'amax1@etf.unsa.ba', date: '21.01.2020. 11:33', action: 'Request: 19N0 - DECLINED' },
  { id: 27, userId: 17117, username: 'amax1', email: 'amax1@etf.unsa.ba', date: '21.01.2020. 12:03', action: 'Request: 90K1 - APPROVED' },
  { id: 28, userId: 99123, username: 'fwander1', email: 'fwander1@etf.unsa.ba', date: '21.01.2020. 12:08', action: 'Request: Tutor - RM' },

];
  ngOnInit(): void {
  }

}
