import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from '../admin.service';
import { PopupService } from 'src/core/popup/popup.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
  })

export class UsersComponent implements OnInit {

    newUserForm: FormGroup;
    filteredusers = [];
    role;
    loginSuccess = false;
    roles = [];
    users = [];
    search = '';

    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private adminService: AdminService,
        private popupService: PopupService) {
    }

    ngOnInit() {
        this.adminService.getAllRoles().subscribe(res => {
            this.roles = res;
            this.role = res[0];
        });
        this.newUserForm = this.formBuilder.group({
            firstName: [null, Validators.required],
            lastName: [null, Validators.required],
            email: [null, Validators.required],
            username: [null, Validators.required],
            password: [null, Validators.required],
        });
    }
    getUsers() {
        this.adminService.getAllUsers().subscribe(res => {
            this.users = res;
            this.filter(this.search);
        });
    }
    filter(event) {
        this.filteredusers = this.users.filter(x => x.email.indexOf(this.search)!=-1 || 
        x.credentials.role.description.indexOf(this.search) != -1);
    }

    public submit(): void {
        const {firstName, lastName, email, username, password} = this.newUserForm.value;
        this.adminService.createNewUser({firstName, lastName, email}, {username, password, role: this.role})
        .subscribe(res => {
            this.newUserForm.reset();
        });
    }
    changeRole(user) {
        this.popupService.changeRolePopup(user);
    }
}
