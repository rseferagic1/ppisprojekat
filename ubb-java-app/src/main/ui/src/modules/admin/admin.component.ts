import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from './admin.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
  })

export class AdminComponent implements OnInit {

    newUserForm: FormGroup;
    role;
    loginSuccess = false;
    roles = [];
    users = [];

    links: any[] = [
        {
          path: 'home',
          label: 'Home'
        },
        {
          path: 'users',
          label: 'Users'
        },
        {
            path: 'roles',
            label: 'Roles'
        },
        {
          path: 'requests',
          label: 'Requests'
        },
        {
          path: 'account-statistics',
          label: 'Accounts Statistics'
        },
        {
          path: 'request-statistics',
          label: 'Request Statistics'
        },
        
        {
          path: 'access-management',
          label: 'Access Management'
        }, 
        {
          path: 'history',
          label: 'Request History'
        }
      ];
    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private adminService: AdminService) {
    }

    ngOnInit() {
        this.adminService.getAllRoles().subscribe(res => {
            this.roles = res;
            this.role = res[0];
        });
        this.adminService.getAllUsers().subscribe(res => {
            this.users = res;
        });
        this.newUserForm = this.formBuilder.group({
            firstName: [null, Validators.required],
            lastName: [null, Validators.required],
            email: [null, Validators.required],
            username: [null, Validators.required],
            password: [null, Validators.required],
        });
    }

    public submit(): void {

    }
}
