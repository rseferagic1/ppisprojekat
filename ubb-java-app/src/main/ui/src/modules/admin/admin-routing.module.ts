import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';
import { AdminRequestsComponent } from './admin-requests/admin-requests.component';
import { StatisticsComponent } from './statistics/statistics/statistics.component';
import { RequestStatisticsComponent } from './statistics/request-statistics/request-statistics.component';
import { HistoryComponent } from './history/history.component';
import { AccessManagementComponent } from './access-management/access-management.component';

const adminRoutes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: AdminHomeComponent
            },
            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'roles',
                component: RolesComponent
            },
            {
                path: 'requests',
                component: AdminRequestsComponent
            },
            {
                path: 'account-statistics',
                component: StatisticsComponent
            },
            {
                path: 'request-statistics',
                component: RequestStatisticsComponent
            },
            {
                path: 'history',
                component: HistoryComponent
            },
            {
                path: 'access-management',
                component: AccessManagementComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
