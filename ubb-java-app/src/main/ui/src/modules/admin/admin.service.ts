import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RoleDTO } from 'src/core/models/role-dto';
import { UbbUserDTO } from 'src/core/models/ubb-user-dto';
import { RequestDTO } from 'src/core/models/request-dto';
import { PermissionDTO } from 'src/core/models/permission-dto';

@Injectable()
export class AdminService {

    constructor(
      protected http: HttpClient
    ) {}

    public getAllRoles(): Observable<Array<RoleDTO>> {
        return this.http.get<Array<RoleDTO>>(environment.url +  'api/roles');
    }
    public getAllPermissions(): Observable<Array<PermissionDTO>> {
        return this.http.get<Array<PermissionDTO>>(environment.url +  'api/roles/permissions');
    }
    public getPermissionsByRoleId(roleId): Observable<Array<PermissionDTO>> {
        return this.http.get<Array<PermissionDTO>>(environment.url +  `api/roles/${roleId}/permissions`);
    }

    public getAllUsers(): Observable<Array<UbbUserDTO>> {
        return this.http.get<Array<UbbUserDTO>>(environment.url +  'api/users');
    }
    public createNewUser(user: any, credentials: any): any {
        return this.http.post<any>(environment.url + 'api/users', {user, credentials});
    }
    public createNewRole(role: string): any {
        return this.http.post<any>(environment.url + 'api/roles', role);
    }
    
    public createNewPermission(role: string): any {
        return this.http.post<any>(environment.url + 'api/roles/permission', role);
    }
    public updateCredentials(credentials: any) {
        return this.http.post<any>(environment.url + 'api/credentials', credentials);
    }
    public getRequests(): Observable<RequestDTO[]> {
        return this.http.get(`${environment.url}api/requests`) as Observable<RequestDTO[]>;
    }
    public batchUpdateRequests(status: string, requests: RequestDTO[]): Observable<void> {
        return this.http.put(`${environment.url}api/requests/${status}`, requests) as unknown as Observable<void>;
    }
}
