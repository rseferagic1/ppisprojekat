import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-request-statistics',
  templateUrl: './request-statistics.component.html',
  styleUrls: ['./request-statistics.component.scss']
})
export class RequestStatisticsComponent implements OnInit {

  constructor() { }
  public options: any = {
    chart: {
      type: 'pie',
    },
    title: {
      text: 'Request Statistics'
    },
    subtitle: {
      text: 'Date range: 01.05.2020. - 01.06.2020.'
    },
    accessibility: {
      point: {
          valueSuffix: '%'
      }
  },
    plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          colors: this.pieColors(),
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
              distance: -50,
              filter: {
                  property: 'percentage',
                  operator: '>',
                  value: 4
              }
          }
      }
  },
  series: [{
      name: 'Requests',
      data: [
          { name: 'Approved', y: 44 },
          { name: 'On hold', y: 7 },
          { name: 'Open', y: 52 },
          { name: 'Declined', y: 17 }
      ]
  }]
  }
  public admin: any = {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Admin Response Statistics'
    },
    subtitle: {
      text: 'Date range: 01.05.2020. - 01.06.2020.'
    },
    accessibility: {
      point: {
          valueSuffix: '%'
      }
  },
    plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          colors: this.pieColors(),
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
              distance: -50,
              filter: {
                  property: 'percentage',
                  operator: '>',
                  value: 4
              }
          }
      }
  },
  series: [{
    name: 'FWANDER1',
    data: [26]

}, {
    name: 'AMAX1',
    data: [16]
},{
name: 'LLECILE2',
data: [14]}]
  }
  ngOnInit(): void {
    
    Highcharts.chart('reqcontainer', this.options);
    Highcharts.chart('admin', this.admin);
  }
  pieColors() {
    var colors = [],
        base = Highcharts.getOptions().colors[3],
        i;

    for (i = 0; i < 10; i += 1) {
        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}
}
