import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  constructor() { }
  public options: any = {
    chart: {
      type: 'pie',
    },
    title: {
      text: 'Accounts Statistics'
    },
    accessibility: {
      point: {
          valueSuffix: '%'
      }
  },
    plotOptions: {
      pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          colors: this.pieColors(),
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
              distance: -50,
              filter: {
                  property: 'percentage',
                  operator: '>',
                  value: 4
              }
          }
      }
  },
  series: [{minPointSize: 10,
    innerSize: '20%',
    zMin: 0,
      name: 'Accounts',
      data: [
          { name: 'Professors', y: 64 },
          { name: 'Administrators', y: 4 },
          { name: 'Students', y: 1900 },
          { name: 'Student Administrators', y: 7 },
          { name: 'Tutors', y: 40 }
      ]
  }]
  }
  public active : any = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Active / Inactive Students'
    },
    subtitle: {
        text: 'Inactivity period: 6 months'
    },
    xAxis: {
        categories: [
            'Students',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: '#'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Active',
        data: [1900]

    }, {
        name: 'Inactive',
        data: [7400]

    }]
};
public activeprof : any = {
  chart: {
      type: 'column'
  },
  title: {
      text: 'Active / Inactive Professors'
  },
  subtitle: {
      text: 'Inactivity period: 6 months'
  },
  xAxis: {
      categories: [
          'Professors',
      ],
      crosshair: true
  },
  yAxis: {
      min: 0,
      title: {
          text: '#'
      }
  },
  tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
      footerFormat: '</table>',
      shared: true,
      useHTML: true
  },
  plotOptions: {
      column: {
          pointPadding: 0.2,
          borderWidth: 0
      }
  },
  series: [{
      name: 'Active',
      data: [92]

  }, {
      name: 'Inactive',
      data: [64]

  }]
};
  
  ngOnInit(): void {
    
    Highcharts.chart('container', this.options);
    Highcharts.chart('active', this.active);
    Highcharts.chart('activeprof', this.activeprof);
  }
  pieColors() {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}


}