import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UbbUserDTO } from 'src/core/models/ubb-user-dto';
import { UserService } from 'src/core/user.service';
import { CourseDTO } from 'src/core/models/course-dto';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ClassroomDTO } from 'src/core/models/classroom-dto';
import { RequestDTO } from 'src/core/models/request-dto';
import { SubmitRequestDTO } from 'src/core/models/submit-request-dto';

@Injectable()
export class StudentService {

    constructor(
        private userService: UserService,
        private http: HttpClient
    ) {}

    public getUsername(): string {
        return this.userService.username;
    }

    public getStudentDetails(): Observable<UbbUserDTO> {
        return this.userService.getUserDetails();
    }

    public getCourses(): Observable<CourseDTO[]> {
        return this.http.get(`${environment.url}api/courses`) as Observable<CourseDTO[]>;
    }

    public getRequests(): Observable<RequestDTO[]> {
        return this.http.get(`${environment.url}api/users/requests/${this.userService.username}`) as Observable<RequestDTO[]>;
    }

    public saveRequest(body: SubmitRequestDTO): Observable<RequestDTO> {
        return this.http.post(`${environment.url}api/requests`, body) as Observable<RequestDTO>;
    }
}
