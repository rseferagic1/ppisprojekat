import { Component, OnInit } from '@angular/core';
import { UbbUserDTO } from 'src/core/models/ubb-user-dto';
import { StudentService } from '../student.service';
import { CourseDTO } from 'src/core/models/course-dto';

@Component({
  selector: 'app-student-account',
  templateUrl: './student-account.component.html',
  styleUrls: ['./student-account.component.scss']
})
export class StudentAccountComponent implements OnInit {

  courseList: Array<CourseDTO> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
