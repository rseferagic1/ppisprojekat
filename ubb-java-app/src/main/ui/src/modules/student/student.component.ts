import { Component, OnInit } from '@angular/core';
import { StudentService } from './student.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs/operators';
import { UsersComponent } from '../admin/users/users.component';
import { UserService } from 'src/core/user.service';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {

  links: any[] = [
    {
      path: 'home',
      label: 'Home'
    },
    {
      path: 'account',
      label: 'My Account'
    },
    {
      path: 'requests',
      label: 'Requests'
    }
  ];

  constructor(
    private userService: UserService
  ) {}

  ngOnInit() {
    if (this.userService.roleId === "103") {
      this.links.push({path: 'tutor', label: 'Tutor class'});
    }
  }

}
