import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentRoutingModule } from './student-routing.module';
import { StudentHomeComponent } from './student-home/student-home.component';
import { StudentComponent } from './student.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCommonModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { StudentAccountComponent } from './student-account/student-account.component';
import { StudentRequestsComponent } from './student-requests/student-requests.component';
import { MatButtonModule } from '@angular/material/button';
import { StudentService } from './student.service';
import { CoreModule } from 'src/core/core.module';
import { UserDetailsModule } from '../widgets/user-details/user-details.module';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { TutorComponent } from './tutor/tutor.component';

@NgModule({
  declarations: [
    StudentHomeComponent,
    StudentComponent,
    StudentAccountComponent,
    StudentRequestsComponent,
    TutorComponent
  ],
  imports: [
    CommonModule,
    StudentRoutingModule,
    MatTabsModule,
    MatCommonModule,
    MatToolbarModule,
    MatButtonModule,
    CoreModule,
    UserDetailsModule,
    MatCardModule,
    MatRadioModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [
    StudentService
  ]
})
export class StudentModule { }
