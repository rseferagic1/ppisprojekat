import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { StudentHomeComponent } from './student-home/student-home.component';
import { StudentComponent } from './student.component';
import { StudentAccountComponent } from './student-account/student-account.component';
import { StudentRequestsComponent } from './student-requests/student-requests.component';
import { TutorComponent } from './tutor/tutor.component';

const studentRoutes: Routes = [
    {
        path: '',
        component: StudentComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: StudentHomeComponent
            },
            {
                path: 'account',
                component: StudentAccountComponent
            },
            {
                path: 'requests',
                component: StudentRequestsComponent
            },
            {
                path: 'tutor',
                component: TutorComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(studentRoutes)
    ],
    exports: [RouterModule]
})
export class StudentRoutingModule {}
