import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { CourseDTO } from 'src/core/models/course-dto';
import { ClassroomDTO } from 'src/core/models/classroom-dto';
import { DynamicValidators } from 'src/core/util/custom-validators';
import { TableHeader } from 'src/core/models/table-header';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { readProperty } from 'src/core/util/helper-functions';
import { forkJoin } from 'rxjs';
import { RequestDTO } from 'src/core/models/request-dto';
import { SubmitRequestDTO } from 'src/core/models/submit-request-dto';
import { concatMap } from 'rxjs/operators';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-requests',
  templateUrl: './student-requests.component.html',
  styleUrls: ['./student-requests.component.scss']
})
export class StudentRequestsComponent implements OnInit {

  readProperty: (obj: any, field: string) => any = readProperty;
  formGroup: FormGroup;
  courseList: Array<CourseDTO> = [];
  otherSelected = false;

  columnDef: TableHeader[] = [
    { field: 'requestType', headerName: 'Request' },
    { field: 'requestDate', headerName: 'Requested at'},
    { field: 'requestStatus', headerName: 'Status'},
  ];

  requestTypeOptions: any[] = [
    { key: 1,  value: 'Tutor of specified course', checked: true },
    { key: 2,  value: 'Enroll in specified course', checked: false },
    { key: 3,  value: 'Other', checked: false }
  ];

  dataSource: MatTableDataSource<RequestDTO>;
  displayedColumns: string[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private studentService: StudentService
  ) {
    this.formGroup = new FormGroup({
      type: new FormControl(this.requestTypeOptions[0].key, Validators.required),
      requestType: new FormControl(this.requestTypeOptions[0].value),
      course: new FormControl(null, [DynamicValidators.required([
        {
            controlName: 'type',
            rule: `value != undefined && value < 3`
        }
      ])]),
      other: new FormControl(null, [Validators.maxLength(50), DynamicValidators.required([
        {
            controlName: 'type',
            rule: `value != undefined && value == 3`
        }
      ])])
    });
  }

  private get requestType(): AbstractControl {
    return this.formGroup.get('requestType');
  }

  ngOnInit(): void {
    this.displayedColumns = this.columnDef.map(column => column.field);

    forkJoin([
      this.studentService.getCourses(),
      this.studentService.getRequests(),
    ]).subscribe(
      (results: [CourseDTO[], RequestDTO[]]) => {
        this.courseList = results[0];
        this.dataSource = new MatTableDataSource<RequestDTO>(results[1]);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  public onSubmit(): void {
    if (!this.formGroup.valid) {
      return;
    }

    // if reason is 3 - Other, set requestType to input field value
    if (this.formGroup.get('type').value === 3) {
      this.requestType.setValue(this.formGroup.get('other').value);
    }

    const requestBody: SubmitRequestDTO = {
      ...this.formGroup.value,
      username: this.studentService.getUsername()
    };

    this.studentService.saveRequest(requestBody).pipe(
      concatMap(
        (request: RequestDTO) => this.studentService.getRequests()
      )
    ).subscribe(
      (res: RequestDTO[]) => {
        this.dataSource = new MatTableDataSource<RequestDTO>(res);
        this.dataSource.paginator = this.paginator;
        this.formGroup.reset();
      }
    );
  }

  public onChange(event: any) {
    this.otherSelected = event.value === 3;
    this.requestType.setValue(this.requestTypeOptions[event.value - 1].value);
  }

}
