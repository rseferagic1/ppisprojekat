import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/core/user.service';
import { UbbUserDTO } from 'src/core/models/ubb-user-dto';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  @Input() title?: string;

  userDetails: UbbUserDTO;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getUserDetails().subscribe(
      (res: UbbUserDTO) => {
        this.userDetails = res;
      }
    );
  }

}
