import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorRequestsArchiveComponent } from './professor-requests-archive.component';

describe('ProfessorRequestsComponent', () => {
  let component: ProfessorRequestsArchiveComponent;
  let fixture: ComponentFixture<ProfessorRequestsArchiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorRequestsArchiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorRequestsArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
