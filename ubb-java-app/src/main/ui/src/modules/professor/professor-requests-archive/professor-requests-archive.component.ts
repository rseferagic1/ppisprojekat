import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfessorService } from '../professor.service';
import { TableHeader } from 'src/core/models/table-header';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { readProperty } from 'src/core/util/helper-functions';
import { RequestDTO } from 'src/core/models/request-dto';


@Component({
  selector: 'app-professor-requests-archive',
  templateUrl: './professor-requests-archive.component.html',
  styleUrls: ['./professor-requests-archive.component.scss']
})
export class ProfessorRequestsArchiveComponent implements OnInit {

  readProperty: (obj: any, field: string) => any = readProperty;

  columnDef: TableHeader[] = [
    { field: 'studentName', headerName: 'Requested by' },
    { field: 'requestDate', headerName: 'Requested at'},
    { field: 'requestType', headerName: 'Request type'},
    { field: 'requestStatus', headerName: 'Status'},
  ];

  dataSource: MatTableDataSource<RequestDTO>;
  displayedColumns: string[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private professorService: ProfessorService
  ) {}

  ngOnInit() {
    this.displayedColumns = this.columnDef.map(column => column.field);
    this.professorService.getStudentRequests().subscribe(
      (res: RequestDTO[]) => {
        this.dataSource = new MatTableDataSource<RequestDTO>(res);
        this.dataSource.paginator = this.paginator;
      }
    );
  }
}
