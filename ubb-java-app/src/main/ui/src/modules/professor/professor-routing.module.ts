import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProfessorComponent } from './professor.component';
import { ProfessorRequestsComponent } from './professor-requests/professor-requests.component';
import { ProfessorHomeComponent } from './professor-home/professor-home.component';
import { ProfessorRequestsArchiveComponent } from './professor-requests-archive/professor-requests-archive.component';
import { ProfessorAccountComponent } from './professor-account/professor-account.component';



const professorRoutes: Routes = [
    {
        path: '',
        component: ProfessorComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: ProfessorHomeComponent
            },
            {
                path: 'account',
                component: ProfessorAccountComponent
            },
            {
                path: 'requests',
                component: ProfessorRequestsComponent
            },
            {
                path: 'student-requests',
                component: ProfessorRequestsArchiveComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(professorRoutes)
    ],
    exports: [RouterModule]
})
export class ProfessorRoutingModule {}
