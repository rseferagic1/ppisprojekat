import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfessorRoutingModule } from './professor-routing.module';
import { ProfessorHomeComponent } from './professor-home/professor-home.component';
import { ProfessorComponent } from './professor.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCommonModule } from '@angular/material/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ProfessorRequestsComponent } from './professor-requests/professor-requests.component';
import { ProfessorRequestsArchiveComponent } from './professor-requests-archive/professor-requests-archive.component';
import { MatButtonModule } from '@angular/material/button';
import { ProfessorService } from './professor.service';
import { CoreModule } from 'src/core/core.module';
import { UserDetailsModule } from '../widgets/user-details/user-details.module';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ProfessorAccountComponent } from './professor-account/professor-account.component';


@NgModule({
  declarations: [
    ProfessorHomeComponent,
    ProfessorComponent,
    ProfessorRequestsComponent,
    ProfessorRequestsArchiveComponent,
    ProfessorAccountComponent
  ],
  imports: [
    CommonModule,
    ProfessorRoutingModule,
    MatTabsModule,
    MatCommonModule,
    MatToolbarModule,
    MatButtonModule,
    CoreModule,
    UserDetailsModule,
    MatCardModule,
    MatRadioModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [
    ProfessorService
  ]
})
export class ProfessorModule { }
