import { Component, OnInit } from '@angular/core';
import { ProfessorService } from './professor.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-professor',
  templateUrl: './professor.component.html',
  styleUrls: ['./professor.component.scss']
})
export class ProfessorComponent implements OnInit {

  links: any[] = [
    {
      path: 'home',
      label: 'Home'
    },
    {
      path: 'account',
      label: 'My Account'
    },
    {
      path: 'requests',
      label: 'Requests'
    },
    {
      path: 'student-requests',
      label: 'Student Requests'
    }
  ];

  constructor() {}

  ngOnInit() {
  }

}
