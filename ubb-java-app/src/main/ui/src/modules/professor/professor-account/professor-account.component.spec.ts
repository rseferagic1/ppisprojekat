import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorAccountComponent } from './professor-account.component';

describe('ProfessorAccountComponent', () => {
  let component: ProfessorAccountComponent;
  let fixture: ComponentFixture<ProfessorAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
