import { Component, OnInit } from '@angular/core';
import { CourseDTO } from 'src/core/models/course-dto';

@Component({
  selector: 'app-professor-account',
  templateUrl: './professor-account.component.html',
  styleUrls: ['./professor-account.component.scss']
})
export class ProfessorAccountComponent implements OnInit {

  courseList: Array<CourseDTO> = [];

  constructor() { }

  ngOnInit(): void {
  }

}
