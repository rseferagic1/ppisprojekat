import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UbbUserDTO } from 'src/core/models/ubb-user-dto';
import { UserService } from 'src/core/user.service';
import { CourseDTO } from 'src/core/models/course-dto';
import { ClassroomDTO } from 'src/core/models/classroom-dto';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { RequestDTO } from 'src/core/models/request-dto';
import { SubmitRequestDTO } from 'src/core/models/submit-request-dto';

@Injectable()
export class ProfessorService {

    constructor(
        private userService: UserService,
        private http: HttpClient
    ) {}

    public getUsername(): string {
        return this.userService.username;
    }

    public getProfessorDetails(): Observable<UbbUserDTO> {
        return this.userService.getUserDetails();
    }

    public getCourses(): Observable<CourseDTO[]> {
        return this.http.get(`${environment.url}api/courses`) as Observable<CourseDTO[]>;
    }
    public getClassrooms(): Observable<ClassroomDTO[]> {
        return this.http.get(`${environment.url}api/classrooms/available`) as Observable<ClassroomDTO[]>;
    }

    public getRequests(): Observable<RequestDTO[]> {
        return this.http.get(`${environment.url}api/users/requests/${this.userService.username}`) as Observable<RequestDTO[]>;
    }

    public saveRequest(body: SubmitRequestDTO): Observable<RequestDTO> {
        return this.http.post(`${environment.url}api/requests`, body) as Observable<RequestDTO>;
    }

    public getStudentRequests(): Observable<RequestDTO[]> {
        // tslint:disable-next-line: max-line-length
        return this.http.get(`${environment.url}api/requests/course-requests/professor/${this.userService.username}`) as Observable<RequestDTO[]>;
    }
}
