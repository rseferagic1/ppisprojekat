import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { CourseDTO } from 'src/core/models/course-dto';
import { ClassroomDTO } from 'src/core/models/classroom-dto';
import { ProfessorService } from '../professor.service';
import { DynamicValidators } from 'src/core/util/custom-validators';
import { TableHeader } from 'src/core/models/table-header';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { readProperty } from 'src/core/util/helper-functions';
import { forkJoin } from 'rxjs';
import { RequestDTO } from 'src/core/models/request-dto';
import { SubmitRequestDTO } from 'src/core/models/submit-request-dto';
import { concatMap } from 'rxjs/operators';


@Component({
  selector: 'app-professor-requests',
  templateUrl: './professor-requests.component.html',
  styleUrls: ['./professor-requests.component.scss']
})
export class ProfessorRequestsComponent implements OnInit {

  readProperty: (obj: any, field: string) => any = readProperty;
  formGroup: FormGroup;
  courseList: Array<CourseDTO> = [];
  classroomList: Array<ClassroomDTO> = [];
  showClassroom = false;
  otherSelected = false;

  columnDef: TableHeader[] = [
    { field: 'requestType', headerName: 'Request' },
    { field: 'requestDate', headerName: 'Requested at'},
    { field: 'requestStatus', headerName: 'Status'},
  ];

  requestTypeOptions: any[] = [
    { key: 1,  value: 'Professor of specified course', checked: true },
    { key: 2,  value: 'Reserve classroom for the exam', checked: false },
    { key: 3,  value: 'Other', checked: false }
  ];

  dataSource: MatTableDataSource<RequestDTO>;
  displayedColumns: string[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private professorService: ProfessorService
  ) {
    this.formGroup = new FormGroup({
      type: new FormControl(this.requestTypeOptions[0].key, Validators.required),
      requestType: new FormControl(this.requestTypeOptions[0].value),
      course: new FormControl(null, [DynamicValidators.required([
        {
            controlName: 'type',
            rule: `value != undefined && value < 3`
        }
      ])]),
      classroom: new FormControl(null, [DynamicValidators.required([
        {
            controlName: 'type',
            rule: `value != undefined && value == 2`
        }
      ])]),
      other: new FormControl(null, [Validators.maxLength(50), DynamicValidators.required([
        {
            controlName: 'type',
            rule: `value != undefined && value == 3`
        }
      ])])
    });
  }

  private get requestType(): AbstractControl {
    return this.formGroup.get('requestType');
  }

  ngOnInit(): void {
    this.displayedColumns = this.columnDef.map(column => column.field);

    forkJoin([
      this.professorService.getCourses(),
      this.professorService.getRequests(),
      this.professorService.getClassrooms()
    ]).subscribe(
      (results: [CourseDTO[], RequestDTO[], ClassroomDTO[]]) => {
        this.courseList = results[0];
        this.classroomList = results[2];
        this.dataSource = new MatTableDataSource<RequestDTO>(results[1]);
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  public onSubmit(): void {
    if (!this.formGroup.valid) {
      return;
    }

    // if reason is 3 - Other, set requestType to input field value
    if (this.formGroup.get('type').value === 3) {
      this.requestType.setValue(this.formGroup.get('other').value);
    }

    const requestBody: SubmitRequestDTO = {
      ...this.formGroup.value,
      username: this.professorService.getUsername()
    };

    this.professorService.saveRequest(requestBody).pipe(
      concatMap(
        (request: RequestDTO) => this.professorService.getRequests()
      )
    ).subscribe(
      (res: RequestDTO[]) => {
        this.dataSource = new MatTableDataSource<RequestDTO>(res);
        this.dataSource.paginator = this.paginator;
        this.formGroup.reset();
      }
    );
  }

  public onChange(event: any) {
    this.showClassroom = event.value === 2;
    this.otherSelected = event.value === 3;
    this.requestType.setValue(this.requestTypeOptions[event.value - 1].value);
  }
}

