import { Rule, Group, RequiredValidator } from './dynamic-validator';
import { ValidatorFn, AbstractControl } from '@angular/forms';

export class DynamicValidators {

    static required(rules?: Array<Rule | Group>): ValidatorFn {
      return (c: AbstractControl) => {
        let validator = c[RequiredValidator.name];
        if (!validator) {
          validator = c[RequiredValidator.name] = new RequiredValidator(rules);
        }

        return validator.validate(c);
      };
    }
}
