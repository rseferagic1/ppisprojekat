import { AbstractControl, FormGroup, Validator, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';

// tslint:disable no-string-literal
// tslint:disable: variable-name

export enum Relation {
    and,
    or
}

export type EvalFn = (value: any) => boolean;

export interface Rule {
    controlName: string;
    eval?: string | EvalFn;
    rule: string | EvalFn;
    relation?: Relation;
}

export interface Group {
    relation?: Relation;
    rules: Array<Rule | Group>;
}

export class DynamicValidator {

    private _dependencyControls: Array<AbstractControl>;
    private _expression: string;
    private _isDynamicControlSetUp: boolean;

    constructor(private readonly _rules: Array<Rule | Group>) {
    }

    private getExpression(control: AbstractControl): string {
        if (this._expression == null || this._expression === undefined) {
            this.evalExpression(control);
        }

        return this._expression;
    }

    private setUpDependencyControls(control: AbstractControl): void {
        if (!this._isDynamicControlSetUp) {
            this._isDynamicControlSetUp = true;
            const dependencyControls = this.getDependencyControls(control);

            for (const dependecyControl of dependencyControls) {
                dependecyControl.valueChanges.subscribe(() => {
                    control.updateValueAndValidity({ emitEvent: false });
                });
            }
        }
    }

    private getDependencyControls(control: AbstractControl): Array<AbstractControl> {
        if (this._dependencyControls == null || this._dependencyControls === undefined) {
            this.evalExpression(control);
        }

        return this._dependencyControls;
    }

    private evalExpression(control: AbstractControl): void {
        this._dependencyControls = [];
        this._expression = '';

        for (const iterator of this._rules) {
            const rule: Rule | Group = iterator;
            const isGroup = rule['controlName'] == null;

            if (isGroup) {
                this.evalGroupExpression(control, rule as Group);
            } else {
                this.evalRuleExpression(control, rule as Rule);
            }
        }
    }

    private evalRuleExpression(control: AbstractControl, rule: Rule): void {
        const c = control.parent.controls[rule.controlName] as FormGroup;
        this._dependencyControls.push(c);

        if (typeof rule.rule === 'string') {
            if (rule.rule.indexOf('data') > -1) {
                rule.eval = rule.rule.replace(new RegExp('data', 'g'), `control.parent.controls['` + rule.controlName + `'].data`);
            }

            if (rule.rule.indexOf('value') > -1) {
                rule.eval = rule.rule.replace(new RegExp('value', 'g'), `control.parent.controls['` + rule.controlName + `'].value`);
            }
            this._expression = `${this._expression}${rule.eval}`;
        } else {
            if (control['evalFns'] == null) {
                control['evalFns'] = [];
            }

            const index = control['evalFns'].length;
            control['evalFns'].push(rule.eval);

            this._expression = this._expression +
                `control['evalFns'][` + index + `](control.parent.controls['` + rule.controlName + `'].value)`;
        }

        switch (rule.relation) {
            case Relation.and:
                this._expression = `${this._expression}' && '`;
                break;
            case Relation.or:
                this._expression = this._expression + ' || ';
                break;
                default:
        }
    }

    private evalGroupExpression(control: AbstractControl, group: Group): void {
        this._expression = this._expression + ' ( ';

        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < group.rules.length; i++) {
            const rule: Rule | Group = group.rules[i];
            const isGroup = rule['controlName'] == null;

            if (isGroup) {
                this.evalGroupExpression(control, rule as Group);
            } else {
                this.evalRuleExpression(control, rule as Rule);
            }
        }

        this._expression = this._expression + ' ) ';

        switch (group.relation) {
            case Relation.and:
                this._expression = this._expression + ' && ';
                break;
            case Relation.or:
                this._expression = this._expression + ' || ';
                break;
                default:
        }
    }

    public validateControl(control: AbstractControl, validationFunc: () => any, applied?: (isApplied: boolean) => void): any {
        if (this._rules == null) {
            if (applied != null) {
                applied(true);
            }

            return validationFunc();
        }
        if (control.parent != null) {
                this.setUpDependencyControls(control);
                // tslint:disable-next-line:no-eval
                const result = eval(this.getExpression(control));

                if (result) {
                    if (applied != null) {
                        applied(true);
                    }

                    return validationFunc();
                }
                if (applied != null) {
                        applied(false);
                    }

                return null;
            }

    }
}

export class RequiredValidator extends DynamicValidator implements Validator {

  constructor(private readonly rules?: Array<Rule | Group>) {
      super(rules);
  }

  validate(control: AbstractControl): Observable<ValidationErrors> | ValidationErrors | null {
      return super.validateControl(control, () => {
          // tslint:disable-next-line:triple-equals
          if (control.value == undefined || control.value === ''
              || ((typeof control.value === 'string') && (control.value).trim() === '')
              || ((control.value instanceof Array) && (control.value).length === 0)) {
              return {
                  required: {
                      current: control.value,
                      required: true
                  }
              };
          }

          return null;
      });
  }
}
