import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './user.service';
import { MatDialogModule } from '@angular/material/dialog';
import { RequestsWarnPopupComponent } from './popup/requests-warn-popup/requests-warn-popup.component';
import { MatButtonModule } from '@angular/material/button';
import { PopupService } from './popup/popup.service';
import { ChangeRolePopupComponent } from './popup/change-role-popup/change-role-popup.component';
import { MatRadioModule, MatRadioGroup } from '@angular/material/radio';
import {FormsModule} from '@angular/forms';
import { ChangePermissionPopupComponent } from './popup/change-permission-popup/change-permission-popup.component';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [RequestsWarnPopupComponent, ChangeRolePopupComponent, ChangePermissionPopupComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
    MatRadioModule,
    FormsModule,
    MatCheckboxModule
  ],
  providers: [
    UserService,
    PopupService
  ]
})
export class CoreModule { }
