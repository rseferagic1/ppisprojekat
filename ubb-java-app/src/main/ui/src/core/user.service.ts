import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UbbUserDTO } from './models/ubb-user-dto';

@Injectable()
export class UserService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) {}

    public get username(): string {
        return localStorage.getItem('username');
    }
    public get roleId(): string {
        return localStorage.getItem('roleId');
    }

    public logOut(): void {
        localStorage.clear();
        this.router.navigateByUrl('login');
    }

    public getUserDetails(): Observable<UbbUserDTO> {
        return this.http.get(`${environment.url}api/users/${this.username}`) as Observable<UbbUserDTO>;
    }
}
