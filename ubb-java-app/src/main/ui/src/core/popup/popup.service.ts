import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { RequestsWarnPopupComponent } from './requests-warn-popup/requests-warn-popup.component';
import { Observable } from 'rxjs';
import { ChangeRolePopupComponent } from './change-role-popup/change-role-popup.component';
import { ChangePermissionPopupComponent } from './change-permission-popup/change-permission-popup.component';

@Injectable()
export class PopupService {

    constructor(
        private matDialog: MatDialog
    ) {}

    public requestsWarnPopup(): Observable<any> {
        let dialogRef: MatDialogRef<RequestsWarnPopupComponent>;

        dialogRef = this.matDialog.open(RequestsWarnPopupComponent, {
            width: '450px',
            height: 'auto',
            disableClose: false
        });

        return dialogRef.afterClosed();
    }
    public changeRolePopup(user): Observable<any> {
        let dialogRef: MatDialogRef<ChangeRolePopupComponent>;

        dialogRef = this.matDialog.open(ChangeRolePopupComponent, {
            width: '450px',
            height: 'auto',
            disableClose: false
        });
        dialogRef.componentInstance.user = user;

        return dialogRef.afterClosed();
    }
    public changePermissionsPopup(role): Observable<any> {
        let dialogRef: MatDialogRef<ChangePermissionPopupComponent>;

        dialogRef = this.matDialog.open(ChangePermissionPopupComponent, {
            width: '450px',
            height: 'auto',
            disableClose: false
        });
        dialogRef.componentInstance.selectedRole = role;

        return dialogRef.afterClosed();
    }
}
