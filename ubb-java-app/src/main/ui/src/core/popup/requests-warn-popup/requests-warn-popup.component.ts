import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-requests-warn-popup',
  templateUrl: './requests-warn-popup.component.html',
  styleUrls: ['./requests-warn-popup.component.scss']
})
export class RequestsWarnPopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<RequestsWarnPopupComponent>
  ) { }

  ngOnInit(): void {
  }

}
