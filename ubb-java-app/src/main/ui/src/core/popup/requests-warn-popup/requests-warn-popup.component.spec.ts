import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsWarnPopupComponent } from './requests-warn-popup.component';

describe('RequestsWarnPopupComponent', () => {
  let component: RequestsWarnPopupComponent;
  let fixture: ComponentFixture<RequestsWarnPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsWarnPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsWarnPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
