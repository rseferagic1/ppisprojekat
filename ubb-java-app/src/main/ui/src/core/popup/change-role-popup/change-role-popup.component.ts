import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from 'src/modules/admin/admin.service';
import { MatDialogRef } from '@angular/material/dialog';
import { MatRadioGroup } from '@angular/material/radio';

@Component({
  selector: 'app-change-role-popup',
  templateUrl: './change-role-popup.component.html',
  styleUrls: ['./change-role-popup.component.scss']
})
export class ChangeRolePopupComponent implements OnInit {
  constructor(
    private adminService: AdminService,
    public dialogRef: MatDialogRef<ChangeRolePopupComponent>
  ) { }

  user;
  selectedRole;
  roles = [];
  ngOnInit(): void {
    this.getRoles();
    this.selectedRole = this.user.credentials.role;
  }
  getRoles() {
    this.adminService.getAllRoles().subscribe(res => {
        this.roles = res;
    });
}
close()  {
  this.dialogRef.close();
}
change() {
  this.user.credentials.role = this.selectedRole;
  this.adminService.updateCredentials(this.user.credentials).subscribe(res => {
    this.dialogRef.close();
  });
}
}
