import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePermissionPopupComponent } from './change-permission-popup.component';

describe('ChangePermissionPopupComponent', () => {
  let component: ChangePermissionPopupComponent;
  let fixture: ComponentFixture<ChangePermissionPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePermissionPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePermissionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
