import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AdminService } from 'src/modules/admin/admin.service';

@Component({
  selector: 'app-change-permission-popup',
  templateUrl: './change-permission-popup.component.html',
  styleUrls: ['./change-permission-popup.component.scss']
})
export class ChangePermissionPopupComponent implements OnInit {

  constructor(
    private adminService: AdminService,
    public dialogRef: MatDialogRef<ChangePermissionPopupComponent>
  ) { }

  user;
  selectedRole;
  roles = [];
  ngOnInit(): void {
    this.getRoles();
  }
  getRoles() {
    this.adminService.getAllPermissions().subscribe(res => {
        this.roles = res;
    });
}
close()  {
  this.dialogRef.close();
}
change() {
}
isChecked(id) {
  if (this.selectedRole.permission.filter(x => x.permissionId === id).length) {
    return true;
  }
  return false;
}
}
