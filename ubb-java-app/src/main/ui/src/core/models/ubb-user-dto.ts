import { CredentialsDTO } from './credentials-dto';

export interface UbbUserDTO {
    userId: number;
    email: string;
    firstName: string;
    lastName: string;
    dateCreated: string;
    credentials: CredentialsDTO;
}
