import { ClassroomDTO } from './classroom-dto';
import { CourseDTO } from './course-dto';

export interface SubmitRequestDTO {
    requestId?: number;
    requestType: string;
    classroom?: ClassroomDTO;
    course?: CourseDTO;
    username: string;
}
