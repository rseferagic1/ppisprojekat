export interface PermissionDTO {
    permissionId: number;
    description: string;
}
