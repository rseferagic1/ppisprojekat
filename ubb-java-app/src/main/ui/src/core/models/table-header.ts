export interface TableHeader {
    field: string;
    headerName: string;
}
