export interface ClassroomDTO {
    classroomId: number;
    name: string;
    description?: string;
    maxNumberOfStudents?: number;
    free?: boolean;
}
