import { UbbUserDTO } from './ubb-user-dto';

export interface RequestDTO {
    requestId: number;
    requestType: string;
    requestStatus: string;
    requestDate: string;
    user: UbbUserDTO;
}
