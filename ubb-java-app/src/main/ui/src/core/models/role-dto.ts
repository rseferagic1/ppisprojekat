import { PermissionDTO } from './permission-dto';

export interface RoleDTO {
    roleId: number;
    description: string;
    permission: Array<PermissionDTO>;
}
