export interface CourseDTO {
    courseId: number;
    name: string;
    description?: string;
    maxNumberOfStudents?: number;
    syllabus?: string;
}
