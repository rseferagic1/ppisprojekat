import { RoleDTO } from './role-dto';

export interface CredentialsDTO {
    credentialId: number;
    username: string;
    isLocked: boolean;
    role: RoleDTO;
    lastLogin: Date;
}
